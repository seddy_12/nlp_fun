#! /usr/bin/env ruby

require "optparse"
require "awesome_print"
require "json"
require "uri"
require "net/http"

class Munger

  def initialize(scores_file:, results_file:, options:)
    scores_file = File.read(scores_file)
    @results_file = File.read(results_file).sub(/Annotation sentiment: null/, "")
    @options = options

    @scores = []
    @reviews = []
    scores_file.split("\n").each do |row|
      all = row.split(",")
      @scores << all.first
      @reviews << all[1..-1].join(",").gsub(/\"/, "")
    end
  end

  def run
    # results = @options[:push] ? results_from_json : normal_results
    results = results_from_json
    print_pretty_thing(results)

    ### Stuff after here will push stuff to elasticsearch
    return unless @options[:push]

    push_to_es
  end

  private

  def pad(string)
    while string.length < 3
      string = " " + string
    end
    string
  end

  def print_pretty_thing(results)
    puts "Number of matching reviews = #{results.select {|r| r[:nlp_classification] == r[:reevoo_sentiment]}.count}"

    neg_neg = pad(results.select {|r| r[:nlp_classification] == "negative" && r[:reevoo_sentiment] == "negative"}.count.to_s)
    neg_neu = pad(results.select {|r| r[:nlp_classification] == "negative" && r[:reevoo_sentiment] == "neutral"}.count.to_s)
    neg_pos = pad(results.select {|r| r[:nlp_classification] == "negative" && r[:reevoo_sentiment] == "positive"}.count.to_s)
    neu_neg = pad(results.select {|r| r[:nlp_classification] == "neutral"  && r[:reevoo_sentiment] == "neutral"}.count.to_s)
    neu_neu = pad(results.select {|r| r[:nlp_classification] == "neutral"  && r[:reevoo_sentiment] == "neutral"}.count.to_s)
    neu_pos = pad(results.select {|r| r[:nlp_classification] == "neutral"  && r[:reevoo_sentiment] == "positive"}.count.to_s)
    pos_neg = pad(results.select {|r| r[:nlp_classification] == "positive" && r[:reevoo_sentiment] == "negative"}.count.to_s)
    pos_neu = pad(results.select {|r| r[:nlp_classification] == "positive" && r[:reevoo_sentiment] == "neutral"}.count.to_s)
    pos_pos = pad(results.select {|r| r[:nlp_classification] == "positive" && r[:reevoo_sentiment] == "positive"}.count.to_s)

    puts "                       Reevoo"
    puts "                | Neg | Neu | Pos"
    puts "     |----------|-----|-----|-----"
    puts "     | Negative | " + neg_neg + " | " + neg_neu + " | " + neg_pos
    puts " NLP | Neutral  | " + neu_neg + " | " + neu_neu + " | " + neu_pos
    puts "     | Postive  | " + pos_neg + " | " + pos_neu + " | " + pos_pos

    # ap results.select {|r| r[:reevoo_sentiment] == "neutral"}
  end

  def nlp_classification(classification)
    classification.gsub(/Very ?/i, "").downcase
  end

  def reevoo_sentiment(score)
    case
    when score <= 6
      "negative"
    when score > 6 && score <= 8
      "neutral"
    when score > 8
      "positive"
    end
  end

  def json_raw
    @json_raw ||= JSON.parse(@results_file);
  end

  def results_from_json
    json_raw["sentences"].map.with_index do |sentence, i|
      {
        sentence: @reviews[i],
        nlp_classification: nlp_classification(sentence["sentiment"]),
        reevoo_score: @scores[i],
        reevoo_sentiment: reevoo_sentiment(@scores[i].to_i)
      }
    end
  end

  def normal_results
    @results_file.split("\n").map.with_index do |row, i|
      output_data = row.split("|")
      {
        sentence: @reviews[i],
        nlp_classification: nlp_classification(output_data[0]),
        reevoo_score: @scores[i],
        reevoo_sentiment: reevoo_sentiment(@scores[i].to_i)
      }
    end
  end

  def push_to_es
    json_raw["sentences"].each_with_index do |review, i|
      review["sentiment"] = nlp_classification(review["sentiment"])
      review["reevoo_sentiment"] = reevoo_sentiment(@scores[i].to_i)
      review["reevoo_score"] = @scores[i].to_i
      review["full_text"] = @reviews[i]

      uri = URI("http://localhost:9200/nlp_initial/sentence")
      req = Net::HTTP::Post.new(uri)
      req.body = JSON.generate(review)
      resp = Net::HTTP.start(uri.hostname, uri.port) {|http| http.request(req)}
      puts "Failed on review #{i}" if !(resp.is_a?(Net::HTTPCreated) || resp.is_a?(Net::HTTPOK))
    end
  end
end

options = {files: [], push: false}

OptionParser.new do |opts|
  opts.banner = "Usage munge_outputs.rb []"
  opts.on("--push", "-p", "Push to elastiscearch") do |p|
    options[:push] = p
  end
end.parse!

if ARGV.any?
  options[:files] = ARGV
  options[:files].each do |file|
    number = file.sub(/.json/, "")
    puts "scores_file: #{"../data/adam_eve/text/batch_2/source_files/source_file_#{number.to_i + 1}"}"
    puts "results_file: #{"../nlp_demo_outputs/#{file}"}"
    Munger.new(
      scores_file: "../data/adam_eve/text/batch_2/source_files/source_file_#{number.to_i + 1}",
      results_file: "../nlp_demo_outputs/#{file}",
      options: options
    ).run
  end
else
  if options[:push]
    results_file = "../nlp_demo_outputs/small_classification.json"
  else
    results_file = "../nlp_demo_outputs/small_classification.out"
  end

  # Test sheeit
  Munger.new(
    scores_file: "../data/adam_eve/test_data/adam_and_eve_raw_score_text_small.csv",
    results_file: results_file,
    options: options
  ).run
end

