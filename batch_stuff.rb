#! /usr/bin/env ruby

files = Dir.foreach("../data/adam_eve/text/batch_2").select {|f| f =~ /^100/}

files.each_with_index do |file, i|
  puts file
  system("javac ReevooAdamEveAnalysisJson.java && java -mx5g ReevooAdamEveAnalysisJson ../data/adam_eve/text/batch_2/#{file} ../nlp_demo_outputs/#{i}.json")
end
